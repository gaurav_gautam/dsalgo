/*Notes:
 * 
 *A class and a structure are the same but in class everything
 *is private by default and vice versa.
 *
 *To access anything that belongs to an object when you have its
 *pointer use `->` but if you have the object then use `.`
 * new gives a pointer to a the newly created instance.
 **/

#include <iostream>
using namespace std;
template<class T> class Node{
//Class to represent a node in the linked list
public:
    //Create the data container of type supplied by template
    //for this node
    T item;
    //Next node
    Node<T> *nextNode;
    
    //Supply constructor for the node
    //For creating a standalone node
    Node(T data){
        this->item = data;
        this->nextNode = NULL;
    }
    //For creating a node and attaching to a provide one
    Node(T data, Node *parent){
        this->item = data;
        parent->nextNode = this;
    }
};

int main(int argc, char *argv[]){
    //Create head node
    Node<float> head(0);
    Node<float> *currNode = &head, *newNode;
    
    //Create a linked list of 10 ints
    for (int i=1; i<10; ++i){
        newNode = new Node<float>(i-0.5, currNode);
        currNode = newNode;
    }
    //Reset currNode back to head node
    currNode = &head;

    //Print the linked list created
    for (int i=0; i<10; ++i){
        cout << currNode->item << " -> ";
        currNode = currNode->nextNode;
    }
    cout << "NULL" << endl;

    return 0;
}